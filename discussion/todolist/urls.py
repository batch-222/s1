from django.urls import path
from . import views

# path(route, view, name) function
# urlpatterns = [
#     path('', views.index, name = 'index'), 
# ]

app_name = "todolist"
urlpatterns = [
    # path('', views.home, name = 'home'),
    path('', views.index, name = 'index'),
    # path('<int:todoitem_id>/', views.todoitem, name = "viewtodoitem"),

    # todolist/register
    path('register', views.register, name="register"),

    # todolist/change_password
    path('change_password', views.change_password, name="change_password"),

    # todolist/login
    path('login', views.login_view, name="login"),

    # todolist/logout
    path('logout', views.logout_view, name="logout"),

    # todolist/add_task
    path('add_task', views.add_task, name="add_task"),

    # todolist/<todoitem_id>
    path('<int:todoitem_id>/', views.todoitem, name="viewtodoitem"),

    # todolist/<todoitem_id>/edit
    path('<int:todoitem_id>/edit', views.update_task, name="update_task"),

    # todolist/<todoitem_id>/delete
    path('<int:todoitem_id>/delete', views.delete_task, name="delete_task"),

    # todolist/add_event
    path('add_event', views.add_event, name="add_event"),
    path('event/<int:event_id>/', views.event, name="viewevent")
]